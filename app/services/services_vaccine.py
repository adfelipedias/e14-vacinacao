from flask import current_app


def register_vaccine(vaccine):
    session = current_app.db.session
    session.add(vaccine)
    session.commit()
