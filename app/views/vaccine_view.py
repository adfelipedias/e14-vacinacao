import sqlalchemy
from app.models.vaccine_model import VaccineModel
from flask import Blueprint, request, jsonify
from datetime import datetime, timedelta

from app.services.services_vaccine import register_vaccine

bp_vaccine = Blueprint("vaccine", __name__)


@bp_vaccine.post("/vaccination")
def create():
    try:
        first_shot = datetime.today().strftime("%Y-%m-%d")
        second_shot = (datetime.today() + timedelta(days=90)).strftime("%Y-%m-%d")

        data = request.get_json()

        if len(data["cpf"]) < 11 or len(data["cpf"]) > 11:
            return {"error": "Invalid CPF"}, 400

        vaccine = VaccineModel(
            cpf=data["cpf"],
            name=data["name"],
            vaccine_name=data["vaccine_name"],
            health_unit_name=data["health_unit_name"],
            first_shot_date=first_shot,
            second_shot_date=second_shot
        )

        register_vaccine(vaccine)
        return jsonify(vaccine), 201

    except sqlalchemy.exc.IntegrityError as err:
        return {"msg": str(err.orig).split("\n")[0]}, 400


@bp_vaccine.get("/vaccination")
def get_all():
    all_data = VaccineModel.query.all()
    return jsonify(all_data)
