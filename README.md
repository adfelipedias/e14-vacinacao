# Esta é uma aplicação para armazenar e registrar cartões de vacinação.

## Tecnologias utilizadas:
- Python
- Flask
- PostgreSQL
- SQLAlchemy
- DataClass
- Blueprint
- Migrations
- Padrão Flask Factory
- Modelo MVS
- Psycopg2
- Python Dotenv

## Rotas:
- POST "/vaccination" - Insere os dados na tabela, o **cpf** deve conter 11 caracteres, o **first_shot_date** é registrado automaticamente com a data atual da requisição e **second_shot_date** é calculado 90 dias após da datade registro.

- GET "/vaccination" - Obtem todos os dados de vacinação.
